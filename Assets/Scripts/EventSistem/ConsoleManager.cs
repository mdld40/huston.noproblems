﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleManager : MonoBehaviour {
	public ConsoleSystemEvent m_consoleReceiver;

	public UnityEngine.UI.Text m_consoleText;

    public UnityEngine.UI.ScrollRect rect;
	// Use this for initialization

	void OnEnable()
	{
		m_consoleReceiver.OnConsoleMsg += OnMsg;
	}

	void OnDisable()
	{
		m_consoleReceiver.OnConsoleMsg += OnMsg;
	}

	// Update is called once per frame
	void OnMsg(string msg)
	{
        if (m_consoleText == null)
            return;

		    m_consoleText.text += "\n" + msg;
            StartCoroutine(UpdateRet());
	}

    IEnumerator UpdateRet()
    {
        yield return null;
        rect.verticalNormalizedPosition = 0;
    }
}
