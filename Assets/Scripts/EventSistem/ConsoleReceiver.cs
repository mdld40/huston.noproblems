﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnConsoleString : UnityEvent<string>{
}

public class ConsoleReciver : MonoBehaviour {

	public ConsoleSystemEvent msgEvent;
	[Space]

	public OnInputString OnMsgSubmit;

	void OnEnable()
	{
		msgEvent.OnConsoleMsg += inputEvent_OnMsgRecived;
	}

	void OnDisable()
	{
		msgEvent.OnConsoleMsg -= inputEvent_OnMsgRecived;
	}


	void inputEvent_OnMsgRecived(string input)
	{
		OnMsgSubmit.Invoke(input);
	}
}
