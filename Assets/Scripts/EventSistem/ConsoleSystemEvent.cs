﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="ConsoleEvent",menuName="System/ConsoleEvent")]
public class ConsoleSystemEvent : ScriptableObject {
	public event InputRecived OnConsoleMsg;

	public void SendMsg(string input){
		OnConsoleMsg(input);
		Debug.Log(input);
	}
}
