﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrewAi : MonoBehaviour {

	public ShipConfigScriptableObject _shipConfig;
	public ModuleScriptableObject[] m_modules {get {return _shipConfig.modules.ToArray(); }}

	public ProblemStatusChangeScriptableObject problemNotifier;
	Dictionary<ModuleScriptableObject, List<OtpionalProblem>> m_troubleList { get { return problemNotifier.m_troubleList; } }
	// Use this for initialization

	public DialogMessage m_dialogData;

	public DialogMessage m_BrifingData;

	DialogItem m_currentDialog;

	public ConsoleSystemEvent m_Console;

    public InputSystemEvent inputEvent;

	int m_brifingMsg;
	bool m_brifingMode = true;

    public void OnDisable()
    {
        inputEvent.OnInputRecived -= ReceiveInput;
		problemNotifier.OnProblemAppear -= WeBrokeIt;
    }

    public void OnEnable()
    {
        inputEvent.OnInputRecived += ReceiveInput;
		problemNotifier.OnProblemAppear += WeBrokeIt;
    }



	void Start()
	{
		SendBrifingMsg ();
	}

	void SendBrifingMsg()
	{
		if (m_BrifingData != null && m_BrifingData.Data.Count > m_brifingMsg) {
			m_currentDialog = m_BrifingData.Data [m_brifingMsg];
			SendDialogMessage (m_currentDialog.Message);
			m_brifingMsg++;
		} else
			StartLevel ();
	}

	void StartLevel()
	{
		m_brifingMode = false;
		GetComponent<LevelManager> ().StartLevel ();
	}

	public void SendRandomMessage()
	{
		//if(m_currentDialog == null)
		{
			
			m_currentDialog = m_dialogData.Data [Random.Range (0, (m_dialogData.Data.Count * 10)-1)/10];
			SendDialogMessage (m_currentDialog.Message);
			if (string.IsNullOrEmpty (m_currentDialog.DefaultAnswer))
				m_currentDialog = null;
		}
	}

	bool TryDialog(string input)
	{
		if (m_currentDialog != null) 
		{
			if (m_currentDialog.Commands != null && m_currentDialog.Commands.Count > 0) 
			{
				foreach (var c in m_currentDialog.Commands) 
				{
					if (input.Contains (c.Command)) 
					{
						SendDialogMessage (c.Answer);
						m_currentDialog = null;
						return true;
					}
				}
			}
			SendDialogMessage (m_currentDialog.DefaultAnswer);
			m_currentDialog = null;
			return true;
		}
		return false;
	}

	public void ReceiveInput(string input)
	{
		if (m_brifingMode) 
		{
			SendBrifingMsg ();
			return;
		}

		input = input.ToLower();


		ModuleScriptableObject fixedModule = null;
		foreach (var m in m_modules) 
		{
			if (input.Contains (m.FixName)) 
			{
				fixedModule = m;
				break;
			}
		}
		
		if(fixedModule == null)
		{
			if(!TryDialog(input))
				NoModule ();
			return;
		}

		input = input.Replace (fixedModule.FixName, " ");
		if (m_troubleList.ContainsKey (fixedModule)) 
		{
			if (m_troubleList [fixedModule].Count > 0) 
			{
				foreach (var p in m_troubleList[fixedModule]) 
				{
					if (input.Contains (p.problem.FixWord)) 
					{
						//m_ship.FixModule (fixedModule, p);
						WeFixIt (fixedModule, p);
						return;
					}
				}
			}
			WrongFix ();
			return;
		}
		WrongModule ();

	}

	void SendDialogMessage(string msg)
	{
		m_Console.SendMsg (msg); 
	}

	public void NoModule()
	{
		SendDialogMessage("Houston didn't undestand you");
	}

	public void WrongModule()
	{
		SendDialogMessage("Houston this module hasn't a problem");
	}

	public void WrongFix()
	{
		SendDialogMessage("Houston wrong fix ");
	}

	public void WeFixIt(ModuleScriptableObject module, OtpionalProblem troubleName)
	{

		if (m_troubleList.ContainsKey (module))
		{
			if (m_troubleList [module].Contains(troubleName)) 
			{
				m_troubleList [module].Remove (troubleName);
				if (m_troubleList [module].Count == 0) 
				{
					m_troubleList.Remove (module);
				}
			}
		}

		problemNotifier.FixProblem(troubleName.problem, module);
		SendDialogMessage("Houston we finaly fix " + module.FixName);
	}

	public void WeBrokeIt(ProblemScriptableObject troubleName, ModuleScriptableObject module )
	{
		SendDialogMessage("Houston we broke " + module.FixName );
	}
}
