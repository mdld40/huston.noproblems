﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct DialogCommandAnswer
{
	public string Command;
	public string Answer;
}
[System.Serializable]
public class DialogItem
{
	public string ItemName;

	public string Message;

	public string DefaultAnswer;

	public List<DialogCommandAnswer> Commands;
}

[CreateAssetMenu(fileName="DialogData",menuName="Content/DialogData")]
public class DialogMessage : ScriptableObject {
	public List<DialogItem> Data;

}
