﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct TroubleStringPair
{
	public TroubleStringPair(ModuleScriptableObject moduleName, OtpionalProblem troubleName)
	{
		module = moduleName;
		trouble = troubleName;
	}
	public ModuleScriptableObject module;
	public OtpionalProblem trouble;
}

public class TroubleGenerator : MonoBehaviour {

	public ShipConfigScriptableObject _shipConfig;
	public ModuleScriptableObject[] m_modules {get {return _shipConfig.modules.ToArray(); }}

	public ProblemStatusChangeScriptableObject problemNotifier;
	Dictionary<ModuleScriptableObject, List<OtpionalProblem>> m_troubleList { get { return problemNotifier.m_troubleList; } }
	// Use this for initialization

    //FIXME 
    public void Awake()
    {
        m_troubleList.Clear();
    }

	public	void BrokeRandomModule()
	{
		TroubleStringPair trouble;
		if(CreateTrouble(out trouble))
		{
			if (!m_troubleList.ContainsKey (trouble.module))
			{
				m_troubleList.Add (trouble.module, new List<OtpionalProblem> ());
			}
			if(!m_troubleList [trouble.module].Contains(trouble.trouble))
			{
				m_troubleList [trouble.module].Add (trouble.trouble);
			}

			problemNotifier.AppearedProblem(trouble.trouble.problem, trouble.module);
		}
	}

	bool CreateTrouble(out TroubleStringPair trouble)
	{
		List<TroubleStringPair> troubleVariants = new List<TroubleStringPair> ();
		foreach (var m in m_modules) 
		{
			if (m_troubleList.ContainsKey(m) && m_troubleList[m].Count >= _shipConfig.MaxTroubleOnModule)
				continue;
			foreach(var t in m.list)
			{
				if (t.active) 
				{
					//if (!m_troubleList.ContainsKey (m)) 
					//{
					//	m_troubleList.Add (m, new List<OtpionalProblem> ());
					//}
					if (!m_troubleList.ContainsKey (m) || !m_troubleList [m].Contains (t)) 
					{
						troubleVariants.Add (new TroubleStringPair (m, t));
					}
				}
			}
		}
		if(troubleVariants.Count > 0)
		{
			int i = Random.Range (0, (troubleVariants.Count * 10)-1)/10;
			trouble = troubleVariants [i];
			return true;
		}
		trouble = new TroubleStringPair (null, null);
		return false;
	}
}
