﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEnd : MonoBehaviour {

	public ProblemStatusChangeScriptableObject problemNotifier;
	public ProgressSystemNotifier progressEvent;

	public void OnEnable()
	{
		problemNotifier.OnProblemFix += problemNotifier_OnProblemFix;
	}

	public void OnDisable()
	{
		problemNotifier.OnProblemFix -= problemNotifier_OnProblemFix;
	}

	void problemNotifier_OnProblemFix(ProblemScriptableObject fixedProblem, ModuleScriptableObject module)
	{
		progressEvent.LevelComplete ();
	}

}
