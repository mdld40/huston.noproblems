﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusOnStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke ("Focus", 0.6f);
		GetComponent<UnityEngine.UI.Button> ().Select ();
	}

	public void Focus()
	{
		GetComponent<UnityEngine.UI.Button> ().Select ();
	}
	// Update is called once per frame
	void Update () {
		
	}
}
