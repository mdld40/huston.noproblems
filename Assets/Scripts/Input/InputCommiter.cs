﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputCommiter : MonoBehaviour {

    public InputSystemEvent inputEvent;
    public UnityEngine.UI.InputField inputField;

    void Start()
    {
        UnityEngine.Assertions.Assert.IsNotNull(inputEvent);
        UnityEngine.Assertions.Assert.IsNotNull(inputField);
    }

    void Update()
    {
        if (Input.GetButtonDown("Submit"))
            PostInput();
		inputField.ActivateInputField ();
    }


    private void PostInput()
    {
        inputEvent.SendInput(inputField.text);
        inputField.text = string.Empty;
    }

}
