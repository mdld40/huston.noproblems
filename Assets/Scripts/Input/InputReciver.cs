﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnInputString : UnityEvent<string>{
}


[System.Serializable]
public class OnInputArray : UnityEvent<string[]>
{
}


public class InputReciver : MonoBehaviour {

    public InputSystemEvent inputEvent;
    [Space]

    public OnInputString OnStringSubmit;

    void OnEnable()
    {
        inputEvent.OnInputRecived += inputEvent_OnInputRecived;
    }

    void OnDisable()
    {
        inputEvent.OnInputRecived -= inputEvent_OnInputRecived;
    }


    void inputEvent_OnInputRecived(string input)
    {
        Debug.Log(input);
        OnStringSubmit.Invoke(input);
    }
}
