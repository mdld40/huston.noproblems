﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="InputEvent",menuName="System/InputEvent")]
public class InputSystemEvent : ScriptableObject {
    public event InputRecived OnInputRecived;

    public void SendInput(string input){
        OnInputRecived(input);
    }
}


public delegate void InputRecived(string input);

