﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDisabler : MonoBehaviour {

    public List<UnityEngine.UI.Button> buttons;

	// Use this for initialization
	void Start () {
        var unlocked = PlayerPrefs.GetInt(Constants.PlayerPrefsLevelKeyword);

        foreach (var button in buttons)
            button.interactable = false;

        for (int i = 0; i <= unlocked; i++)
        {
            buttons[i].interactable = true;
        }
		
	}
}
