﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
	public float m_levelTime;
	public TroubleGenerator m_troubleGenerator;
    public ProgressSystemNotifier progressNotifier;

	public ProblemStatusChangeScriptableObject problemNotifier;

	public float m_flyTime;
	int m_currentTrouble;
	public List <float> m_troubleTime =  new List<float>();
	public CrewAi m_crewAi;


    private bool _levelComplete;
	private bool _levelBrifing = true;

	public float m_MaxIdleTime = 8;
	public float m_MinIdleTime = 4;

	float m_idleTime;

	public int m_messagesBeforeTrouble = 3;
	int m_messagesLeft;


    void Awake()
    {
        UnityEngine.Assertions.Assert.IsNotNull(m_troubleGenerator);
        UnityEngine.Assertions.Assert.IsNotNull(progressNotifier);
		if (m_crewAi == null)
			m_crewAi = GetComponent<CrewAi> ();
		UnityEngine.Assertions.Assert.IsNotNull(m_crewAi);
		if(m_idleTime <= 0.001)
			m_idleTime = Random.Range (m_MinIdleTime, m_MaxIdleTime);
		m_messagesLeft = m_messagesBeforeTrouble;
    }

    public void OnEnable()
    {
        progressNotifier.OnLevelFailed += progressNotifier_OnLevelFailed;
		progressNotifier.OnLevelComplete += progressNotifier_OnLevelFailed;
		problemNotifier.OnProblemAppear += OnProblemAppearEvent;
    }

    void progressNotifier_OnLevelFailed()
    {
        _levelComplete = true;
    }

    public void OnDisable()
    {
        progressNotifier.OnLevelFailed -= progressNotifier_OnLevelFailed;
		progressNotifier.OnLevelComplete -= progressNotifier_OnLevelFailed;
		problemNotifier.OnProblemAppear -= OnProblemAppearEvent;
    }

	public void StartLevel()
	{
		_levelBrifing = false;
	}

    void Update()
    {
		if (_levelComplete || _levelBrifing)
            return;

        progressNotifier.ProgressUpdate(m_flyTime / m_levelTime);

		if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Alpha1))
			m_troubleGenerator.BrokeRandomModule();

		if (Input.GetKey (KeyCode.LeftShift) && Input.GetKeyDown (KeyCode.Alpha2)) 
		{
			m_idleTime = Random.Range (m_MinIdleTime, m_MaxIdleTime);
			m_crewAi.SendRandomMessage ();
		}

		m_flyTime += Time.deltaTime;
        if (m_flyTime >= m_levelTime)
        {
            m_flyTime = m_levelTime;
			if (problemNotifier.m_troubleList.Count == 0 && !_levelComplete) 
			{
				_levelComplete = true;
				progressNotifier.LevelComplete ();
			}
        }


		if (m_currentTrouble < m_troubleTime.Count) 
		{
			if (m_troubleTime [m_currentTrouble] <= m_flyTime)
			{
				m_troubleGenerator.BrokeRandomModule();
				m_currentTrouble++;
			}
		}

		m_idleTime -= Time.deltaTime;
		if (m_idleTime <= 0) 
		{
			if (m_messagesLeft > 0) 
			{
				m_crewAi.SendRandomMessage ();
				m_messagesLeft--;
				m_idleTime = Random.Range (m_MinIdleTime, m_MaxIdleTime);
			} else 
			{
				if (problemNotifier.Problems.Count > 0) {
					m_crewAi.SendRandomMessage ();
					m_idleTime = Random.Range (m_MinIdleTime, m_MaxIdleTime);
				} else 
				{
					m_troubleGenerator.BrokeRandomModule();
					m_messagesLeft = m_messagesBeforeTrouble;
				}
			}
		}

    }


	void OnProblemAppearEvent(ProblemScriptableObject problem, ModuleScriptableObject problemModule)
	{
		SomeEvent ();

	}

	void SomeEvent()
	{
		m_idleTime = Random.Range (m_MinIdleTime, m_MaxIdleTime);
	}

	void RandomMessage()
	{
		
	}

}
