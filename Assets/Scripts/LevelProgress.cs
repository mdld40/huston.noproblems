﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProgress : MonoBehaviour {

    public ProgressSystemNotifier progressNotifier;

    public void OnEnable()
    {
        progressNotifier.OnProgressChanged += notifier_OnProgressChanged;
    }

    public void OnDisable()
    {
        progressNotifier.OnProgressChanged -= notifier_OnProgressChanged;
    }

    void notifier_OnProgressChanged(float progress)
    {
        Vector3 pos = movingItem.position;

        pos.x = Mathf.Lerp(start.transform.position.x, finish.transform.position.x, progress);
        movingItem.position = pos;
    }

	public GameObject start;
	public GameObject finish;
    public Transform movingItem;
	// Use this for initialization
	void Start () {
        Vector3 pos = movingItem.position;
		pos.x = start.transform.position.x;
        movingItem.position = pos;
	}

}
