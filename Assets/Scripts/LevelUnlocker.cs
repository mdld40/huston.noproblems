﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUnlocker : MonoBehaviour {
    
    public int LevelToUnlock = 0;
    public void UnlockLevel() {

      var unlocked =   PlayerPrefs.GetInt(Constants.PlayerPrefsLevelKeyword);
      if (LevelToUnlock > unlocked)
          PlayerPrefs.SetInt(Constants.PlayerPrefsLevelKeyword, LevelToUnlock);
    }

    public void ResetProgress()
    {
        PlayerPrefs.SetInt(Constants.PlayerPrefsLevelKeyword, 0);
    }
}
