﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName="Module",menuName= "Content/Module")]
public class ModuleScriptableObject : ScriptableObject {
    public string FixName;
    public string ModuleDisplayName;
    public Sprite Icon;

    public List<OtpionalProblem> list;
}

[Serializable]
public class OtpionalProblem{
    public  bool active;
    public ProblemScriptableObject problem;
}
