﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="Problem",menuName="Content/Problem")]
public class ProblemScriptableObject : ScriptableObject {
    public string FixWord;
    public Color ProblemColor;
    public Sprite icon;
}
