﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProblemStatusChangeListener : MonoBehaviour {

    public ProblemStatusChangeScriptableObject statusChange;

    public List<ModuleScriptableObject> watchModule;

    public UnityEvent OnProblemAppear;
    public UnityEvent OnProblemFix;


    void Awake() {
        statusChange.OnProblemAppear += statusChange_OnProblemAppear;
        statusChange.OnProblemFix += statusChange_OnProblemFix;
    }

    public void OnDestroy()
    {
        statusChange.OnProblemAppear -= statusChange_OnProblemAppear;
        statusChange.OnProblemFix -= statusChange_OnProblemFix;
    }

    void statusChange_OnProblemAppear(ProblemScriptableObject problem, ModuleScriptableObject module)
    {
        if (watchModule.Contains(module))
            OnProblemAppear.Invoke();
    }

    void statusChange_OnProblemFix(ProblemScriptableObject problem, ModuleScriptableObject module)
    {
        if (watchModule.Contains(module))
            OnProblemFix.Invoke();
    }
}