﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="ProblemStatusChangeSystem",menuName="System/ProblemStatusChange")]
public class ProblemStatusChangeScriptableObject : ScriptableObject {
    public event ProblemUpdate OnProblemAppear;
    public event ProblemUpdate OnProblemFix;
	[System.NonSerialized]
    public Dictionary<ModuleScriptableObject, List<OtpionalProblem>> m_troubleList =
    new Dictionary<ModuleScriptableObject, List<OtpionalProblem>>();

    public List<ProblemScriptableObject> Problems { get {

        var result = new List<ProblemScriptableObject>();

        foreach (var kvp in m_troubleList)
        {
            foreach (var optional in kvp.Value)
                result.Add(optional.problem);
        }
        return result;
    } }

    //public List<ModuleScriptableObject> ProblemModules
    //{
    //    get { 
    //        var result  = new List<ModuleScriptableObject>();
    //        foreach (var kvp in m_troubleList)
    //        {
    //            result.Add(kvp.Key);
    //        }
    //        return result;
    //    }
    //}

    public void AppearedProblem( ProblemScriptableObject problem,ModuleScriptableObject module)
    {
        OnProblemAppear(problem, module);
    }

    public void FixProblem(ProblemScriptableObject problem,ModuleScriptableObject module)
    {
        OnProblemFix(problem ,module);
    }

}

public delegate void ProblemUpdate(ProblemScriptableObject problem,ModuleScriptableObject module);
