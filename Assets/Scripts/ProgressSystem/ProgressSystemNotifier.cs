﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ProgressSystemNotifier", menuName = "System/ProgressSystemNotifier")]
public class ProgressSystemNotifier : ScriptableObject
{
    public event ProgressUpdate OnProgressChanged;
    public event SimpeEvent OnLevelComplete;
    public event SimpeEvent OnLevelFailed;

    public void ProgressUpdate(float newProgress)
    {
        OnProgressChanged(newProgress);
    }
    [ContextMenu("Complete")]
    public void LevelComplete()
    {
        if(OnLevelComplete!=null)
            OnLevelComplete();
    }
    [ContextMenu("Fail")]
    public void FailLevel()
    {
        if (OnLevelFailed != null)
            OnLevelFailed();
    }
}

public delegate void ProgressUpdate(float progress);
public delegate void SimpeEvent();

