﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ScreenSpaceWarning : MonoBehaviour {
    private Image _icon;
    public ProblemStatusChangeScriptableObject problemNotifier;
    public ProblemScriptableObject problem;

    public void Awake()
    {
        _icon = GetComponent<Image>();
        _icon.enabled = false;
    }
    public void OnEnable()
    {
        problemNotifier.OnProblemAppear += problemNotifier_OnProblemAppear;
        problemNotifier.OnProblemFix += problemNotifier_OnProblemFix;
    }

    public void OnDisable()
    {
        problemNotifier.OnProblemAppear -= problemNotifier_OnProblemAppear;
        problemNotifier.OnProblemFix -= problemNotifier_OnProblemFix;
    }


    void problemNotifier_OnProblemFix(ProblemScriptableObject newProblem, ModuleScriptableObject module)
    {
        if (problem == newProblem)
        {
            _icon.enabled = false;
            mySequence.Kill(true);
        }
    }

    Sequence mySequence;
    void problemNotifier_OnProblemAppear(ProblemScriptableObject newProblem, ModuleScriptableObject module)
    {  
        if (problem == newProblem)
        {
            _icon.sprite = newProblem.icon;
            _icon.enabled = true;
            //var tween = DG.

            mySequence = DOTween.Sequence();
            mySequence.Append(_icon.DOBlendableColor(Color.red, 1f));
            mySequence.Append(_icon.DOBlendableColor(Color.white, 1f));
            mySequence.SetLoops(-1);

        }
    }
}
