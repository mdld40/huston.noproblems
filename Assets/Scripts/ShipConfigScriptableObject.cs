﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="ShipCofnig",menuName="Content/ShipConfig")]
public class ShipConfigScriptableObject : ScriptableObject {

    public float ShipHealth = 10;
	public int MaxTroubleOnModule = 1;
    public List<ModuleScriptableObject> modules = new List<ModuleScriptableObject>();
}
