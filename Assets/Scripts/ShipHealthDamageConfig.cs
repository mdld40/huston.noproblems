﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="HealthDamagConfig",menuName="Config/HealthDamageConfig")]
public class ShipHealthDamageConfig : ScriptableObject {
    public float period = 1f;
    public float damagePerModule = 1;
}
