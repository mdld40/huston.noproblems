﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShipHealthHandler : MonoBehaviour {
    
    public ShipConfigScriptableObject shipconfig;
    public ShipHealthDamageConfig shipDamageConfig;
    public ProblemStatusChangeScriptableObject statusNotifier;
    public ProgressSystemNotifier progressNotifier;

    public float CurrentHealth;
    public UnityEngine.UI.Image image;

    private bool UpdateHealth = true;

    public void OnDisable()
    {
        progressNotifier.OnLevelComplete -= progressNotifier_OnLevelComplete;
    }

    public void OnEnable()
    {
        progressNotifier.OnLevelComplete += progressNotifier_OnLevelComplete;
    }

    void progressNotifier_OnLevelComplete()
    {
        UpdateHealth = false;
    }
    

	void Start () {
        CurrentHealth = shipconfig.ShipHealth;
        StartCoroutine(EvaluateDamage());

	}

    public Color StartColor = Color.green;
    public Color EndColor = Color.red;

    IEnumerator EvaluateDamage()
    {
        while (UpdateHealth)
        {
            yield return new WaitForSeconds(shipDamageConfig.period);
            int damage = statusNotifier.Problems.Count;
            var reduceAmmount = damage * shipDamageConfig.damagePerModule;
             CurrentHealth -= reduceAmmount;


            float amount = Mathf.Clamp(CurrentHealth,0,shipconfig.ShipHealth);
            amount /= shipconfig.ShipHealth;
            image.DOFillAmount(amount,0.3f);
            image.DOColor(Color.Lerp(EndColor ,StartColor, amount), 0.5f);

            if (CurrentHealth <= 0)
            {   progressNotifier.FailLevel();
                yield break;
            }

           
        }
    }
}
