﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateChangerListener : MonoBehaviour {

    public ProgressSystemNotifier progressNotifier;

    public UnityEngine.Events.UnityEvent OnLevelFailed;
    public UnityEngine.Events.UnityEvent OnLevelComplete;

    void Awake() {
        progressNotifier.OnLevelComplete += progressNotifier_OnLevelComplete;
        progressNotifier.OnLevelFailed += progressNotifier_OnLevelFailed;
    }

    void progressNotifier_OnLevelFailed()
    {
        OnLevelFailed.Invoke();
    }

    void progressNotifier_OnLevelComplete()
    {
        OnLevelComplete.Invoke();
    }

    public void OnDestroy()
    {
        progressNotifier.OnLevelComplete -= progressNotifier_OnLevelComplete;
        progressNotifier.OnLevelFailed -= progressNotifier_OnLevelFailed;
    }

}
