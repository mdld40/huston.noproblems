﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class ModuleHolder : MonoBehaviour {
    
    public GameObject ModuleTemlate;
    public ShipConfigScriptableObject shipCofnig;
    public Transform nestingTarget;

    public List<ModuleUI> moduleUis;
    public ProblemStatusChangeScriptableObject problemNotifier;

	// Use this for initialization
	void Start () {

        moduleUis = new List<ModuleUI>();

        Assert.IsNotNull(ModuleTemlate);
        Assert.IsNotNull(shipCofnig);
        Assert.IsNotNull(nestingTarget);

        foreach (var module in shipCofnig.modules)
        {
            var UI = Instantiate(ModuleTemlate, nestingTarget);
            var moduleUI = UI.GetComponent<ModuleUI>();
            moduleUI.Setup(module);
            moduleUis.Add(moduleUI);
        }
	}

    public void OnEnable()
    {
        problemNotifier.OnProblemAppear += problemNotifier_OnProblemAppear;
        problemNotifier.OnProblemFix += problemNotifier_OnProblemFix;
    }

    public void OnDisable()
    {
        problemNotifier.OnProblemAppear -= problemNotifier_OnProblemAppear;
        problemNotifier.OnProblemFix -= problemNotifier_OnProblemFix;
    }


    void problemNotifier_OnProblemFix(ProblemScriptableObject fixedProblem, ModuleScriptableObject module)
    {
        var ui = moduleUis.First(item => item.module == module);
        ui.RemoveProblem(fixedProblem);
    }

    void problemNotifier_OnProblemAppear(ProblemScriptableObject problem, ModuleScriptableObject problemModule)
    {
        var ui = moduleUis.First(item => item.module == problemModule);
        ui.AddProblem(problem);
    }
}
