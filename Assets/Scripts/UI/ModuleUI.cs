﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ModuleUI : MonoBehaviour {
    public ModuleScriptableObject module;
    public Image  image;
    public Image moduleProblemColor;
    public Text ModuleName;

    public List<ProblemSlot> problemSlots;

    public void Setup(ModuleScriptableObject mod)
    {
        module = mod;
        ModuleName.text = mod.ModuleDisplayName;
        image.sprite = mod.Icon;
    }

    public void AddProblem(ProblemScriptableObject c)
    {
        var freeSlot = problemSlots.First(item => !item.image.IsActive());
        if (freeSlot == null)
            Debug.LogError("Missing Free Slot");

        freeSlot.problem = c;
        freeSlot.image.sprite = c.icon;
        freeSlot.image.gameObject.SetActive(true);
    }

    public void RemoveProblem(ProblemScriptableObject c)
    {
        var slot = problemSlots.First(item => item.problem == c);
        if (slot == null)
            Debug.LogError("Missing Slot with problem " +c.name);

        slot.image.gameObject.SetActive(false);
    }

    [System.Serializable]
    public class ProblemSlot
    {
        public ProblemScriptableObject problem;
        public Image image;
    }
}
