﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PopUpAppear : MonoBehaviour {

    public float duration=1f;
	public UnityEngine.UI.Button m_focusButton;

    public void OnEnable()
    {
		if(m_focusButton != null)
			m_focusButton.Select ();
        transform.DOShakeScale(duration);
    }

	void Update()
	{
		if(m_focusButton != null)
			m_focusButton.Select ();
	}
}
