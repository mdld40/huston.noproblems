﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class ProblemHolder : MonoBehaviour {
    public ProblemStatusChangeScriptableObject problemNotifier;

    public GameObject ProblemTemplate;
    public List<GameObject> ProblemUIs;
    public Transform nestingTarget;

	void Awake () {
        ProblemUIs = new List<GameObject>();

        Assert.IsNotNull(ProblemTemplate);
        Assert.IsNotNull(problemNotifier);
        Assert.IsNotNull(nestingTarget);
	}

    public void OnEnable()
    {
        problemNotifier.OnProblemAppear += problemNotifier_OnProblemAppear;
        problemNotifier.OnProblemFix += problemNotifier_OnProblemFix;
    }

    public void OnDisable()
    {
        problemNotifier.OnProblemAppear -= problemNotifier_OnProblemAppear;
        problemNotifier.OnProblemFix -= problemNotifier_OnProblemFix;
    }


    void problemNotifier_OnProblemFix(ProblemScriptableObject fixedProblem , ModuleScriptableObject module)
    {
        // problem list still contains this problem? if so , we don't remove from ui
        var last = !problemNotifier.Problems.Contains(fixedProblem);
        if (!last)
            return;

        //last problem in this problem list, remove notification
        var ui = GetUIForProblem(fixedProblem);
        if (ui == null)
        { Debug.LogError("Fixed " + fixedProblem.name + " Missing UI"); return; }

        ProblemUIs.Remove(ui);
        Destroy(ui);
    }

    void problemNotifier_OnProblemAppear(ProblemScriptableObject problem, ModuleScriptableObject module)
    {
        // we check if problem ui already exists if so, return
        var ui = GetUIForProblem(problem);
        if (ui != null)
            return;

        // else create  ui for this problem
        var UI = Instantiate(ProblemTemplate, nestingTarget);
        ProblemUIs.Add(UI);

        var holder = UI.GetComponent<ProblemUI>();
        holder.Setup(problem);
    }

    private GameObject GetUIForProblem(ProblemScriptableObject fixedProblem)
    {
        var ui = ProblemUIs.FirstOrDefault(item => item.GetComponent<ProblemUI>().problem == fixedProblem);
        return ui;
    }
}
