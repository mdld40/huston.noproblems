﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProblemUI : MonoBehaviour {
    public ProblemScriptableObject problem;
    public Image  image;
    public Text ProblemName;
    public Text ProblemFixWord;

    public void Setup(ProblemScriptableObject prob)
    {
        problem = prob;
        ProblemName.text = prob.name;
        ProblemFixWord.text = "("+ prob.FixWord +")";
        image.sprite = prob.icon;
    }
}
